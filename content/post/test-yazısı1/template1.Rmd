---
title: "Yazı Başlığı"
slug: yazi-basligi
date: '2016-11-22'
---

```{r cars, fig.cap='A scatterplot of the cars data'}
par(mar = c(4, 4, .1, .1))
plot(cars, pch = 19, col = 'blue')  # a scatterplot
```
```{r}
library(readr)
realEstate <- read_csv("Sacramentorealestatetransactions.csv")
#library(DT)
price <- summary(realEstate$price)
summary(price)
#datatable(realEstate)
```
```{r}
library(leaflet)
pal <- colorQuantile("RdYlGn", NULL, n = 8) # RColorBrewer palette
leaflet(realEstate) %>% addTiles() %>% addCircleMarkers(lng = ~longitude, lat = ~latitude, color = ~pal(price))
```

